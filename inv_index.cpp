#include "inv_index.h"
#include <iostream>
namespace fs = std::filesystem;
void inv_index::get_file_list() {
  if (fs::exists(filepath)) {
    unsigned int id = 0;
    for (const auto &entry : fs::recursive_directory_iterator(filepath)) {
      bool b = !fs::is_directory(entry);
      if (b) {
        dirs.emplace_back(pair(entry.path().string(), id));
        ++id;
      }
    }
  }
}

string inv_index::read_file(const string &filepath) {
  ifstream f(filepath);
  string text;
  text.assign((istreambuf_iterator<char>(f)), istreambuf_iterator<char>());

  return text;
}

set<string> inv_index::parser(const string &text) {
  stringstream string_stream(text);
  string word;
  set<string> word_dict;
  while (!string_stream.eof()) {
    string_stream >> word;
    if (word == "-")
      continue;
    for (auto &&c : {',', '\"', '.', ')', '(', '{', '}', ':', ';', '!', '?',
                     '@', '\'', '<', '>', '\\', '/'})
      word.erase(remove(word.begin(), word.end(), c), word.end());

    for (auto &c : word) {
      c = tolower(c);
    }

    word_dict.insert(word);
  }
  return word_dict;
}

void inv_index::process(const vector<pair<string, unsigned int>> &s) {
  set<string> dict;

  for (auto indx = 0u; indx < s.size(); ++indx) {
    auto current_file = s.at(indx);
    auto content = read_file(current_file.first);

    dict = parser(content);

    for (const auto &item : dict) {
      m.lock();
      auto position = index.find(item);

      if (position != index.end()) {
        index.at(item).insert(current_file.second);
      } else {
        index.insert(make_pair(item, set{current_file.second}));
      }
      m.unlock();
    }
  }
}

set<unsigned int> inv_index::intersection(const set<unsigned int> &s1,
                                          const set<unsigned int> &s2) {
  set<unsigned int> result;
  for (auto &i : s1)
    if (s2.count(i))
      result.insert(i);
  for (auto &i : s2)
    if (s1.count(i))
      result.insert(i);
  return result;
}

void inv_index::create_inverted_index() {
  get_file_list();

  if (dirs.empty()) {
    puts("Wrong file path!");
    return;
  }

  vector<thread *> threads;

  auto begin = dirs.begin();
  int size = dirs.size();
  int additional = 0;

  for (unsigned int i = 0; i < tnum; ++i) {
    threads.push_back(
        new thread(&inv_index::process, this,
                   vector((i * size / tnum) + begin,
                          ((i + 1) * (size / tnum) + additional) + begin)));
    if (i == tnum - 2) {
      additional = (dirs.size() % size);
    }
  }

  for (unsigned int i = 0; i < tnum; ++i) {
    threads[i]->join();
  }
}
vector<string> inv_index::find(const string &text) {
  auto dict = parser(text);
  vector<set<unsigned int>> result_dirs;
  vector<string> files;

  for (auto word : dict) {
    auto it = index.find(word);
    if (it != index.end()) {
      result_dirs.emplace_back(it->second);
    } else {
      puts("Not found");
      return files;
    }
  }

  while (1 < size(result_dirs)) {
    auto res = intersection(result_dirs.at(0), result_dirs.at(1));
    result_dirs.erase(result_dirs.begin(), result_dirs.begin() + 2);
    result_dirs.insert(result_dirs.begin(), res);
  }

  for (auto i : result_dirs[0]) {
    for (auto j = 0u; j < dirs.size(); ++j) {
      if (i == dirs.at(j).second) {
        files.emplace_back(dirs.at(j).first);
      }
    }
  }

  return files;
}